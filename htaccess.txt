SetEnv HOME_DOMAIN "webarch.net"
SetEnv SITE_TITLE "Webarchitects"
SetEnv STATIC_DIR "/static/"
SetEnv STATIC_TOP "top.shtml"
SetEnv STATIC_BOT "bot.shtml"
SetEnv PAGE_TITLE "Directory Listing"
SetEnv PAGE_DESC  "Directory listing for $SERVER_NAME$REQUEST_URI"
HeaderName /static/top.shtml
ReadmeName /static/bot.shtml

IndexOptions Charset=UTF-8
IndexOptions SuppressHTMLPreamble
IndexOptions SuppressRules
IndexOptions SuppressIcon
IndexOptions SuppressDescription
IndexOptions XHTML
IndexOptions TrackModified

ErrorDocument 404 /static/404.shtml
ErrorDocument 401 /static/401.shtml
ErrorDocument 500 /static/500.shtml
ErrorDocument 403 /static/403.shtml

